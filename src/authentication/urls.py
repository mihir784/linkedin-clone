from django.urls import path, include
from .views import (
    SignUpView,
    SignInView,
    SignOutView,
    PRView,
    PRDone,
    PRConfirm,
    PRComplete
)

urlpatterns = [
    path('', SignInView.as_view(), name='signin_view'),
    path('signup/', SignUpView.as_view(), name='signup_view'),
    path('signout/', SignOutView.as_view(), name='signout_view'),
    path('', include('home.urls')),
    path(
        'password/reset/',
        PRView.as_view(),
        name='password_reset'
    ),
    path(
        'password/reset/done/',
        PRDone.as_view(),
        name='password_reset_done'
    ),
    path(
        'password/reset/confirm/<uidb64>/<token>',
        PRConfirm.as_view(),
        name='password_reset_confirm'
    ),
    path(
        'password/reset/complete/',
        PRComplete.as_view(),
        name='password_reset_complete'
    ),
]
