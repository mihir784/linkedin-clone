from django.contrib import admin
from .models import (
    Post,
    Comment,
    Like,
    Connection,
)


class PostModelAdmin(admin.ModelAdmin):
    model = Post
    list_display = ('text', 'media', 'user', 'created_on', 'updated_on')


class CommentModelAdmin(admin.ModelAdmin):
    model = Comment
    list_display = ('text', 'post', 'user', 'commented_on', 'updated_on')


class LikeModelAdmin(admin.ModelAdmin):
    model = Like
    list_display = ('post', 'user', 'liked_on', 'updated_on')


class ConnectionModelAdmin(admin.ModelAdmin):
    model = Connection
    list_display = (
        'user2',
        'user2',
        'is_requested',
        'is_connected',
        'requested_on',
        'connected_on'
    )


# Register your models here.
admin.site.register(Post, PostModelAdmin)
admin.site.register(Comment, CommentModelAdmin)
admin.site.register(Like, LikeModelAdmin)
admin.site.register(Connection, ConnectionModelAdmin)
