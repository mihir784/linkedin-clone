from django.db.models.query_utils import Q
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import View, TemplateView
from .forms import PostCreateForm, CommentForm
from .models import Connection, Post, Like, Comment
import mimetypes


# Create your views here.
class HomeView(TemplateView):
    template_name = 'home/feed.html'
    form_class = PostCreateForm

    def get(self, request, *arga, **kwargs):
        form = self.form_class()
        posts = Post.objects.all()

        all_posts = []
        file_type = "others"

        for post in posts:
            try:
                file_type = mimetypes.guess_type(post.media.url)[0]
            except Exception:
                pass

            try:
                Like.objects.get(user=request.user, post_id=post.pk)
                is_post_liked = True
            except Exception:
                is_post_liked = False

            all_posts.append({
                'post': post,
                'file_type': file_type,
                'is_post_liked': is_post_liked
            })

        try:
            connections = Connection.objects.filter(
                Q(user2_id=request.user.pk) | Q(user1_id=request.user.pk),
                is_connected=True
            ).count()
        except Exception:
            pass

        context = {
            'form': form,
            'all_posts': all_posts,
            'connections': connections
        }

        return render(request, self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)

        if form.is_valid():
            form.save()
            return redirect('home_view')
        else:
            return redirect('home_view')


class PostDetailView(View):
    template_name = 'home/post_detail.html'
    form_class = PostCreateForm

    def get(self, request, *args, **kwargs):
        post_id = kwargs.get('id')

        try:
            post_obj = Post.objects.get(pk=post_id)
        except Exception:
            return HttpResponse('Post not found')

        try:
            Like.objects.get(user=request.user, post_id=post_id)
            is_post_liked = True
        except Exception:
            is_post_liked = False

        try:
            comments = post_obj.comment_set.filter(parent=None)
        except Exception:
            pass

        comment_form = CommentForm()
        my_context = {
            'is_post_liked': is_post_liked,
            'post': post_obj,
            'comments': comments,
            'comment_form': comment_form,
        }
        return render(request, self.template_name, my_context)

    def post(self, request, *args, **kwargs):

        post_id = kwargs.get('id')
        try:
            post_obj = Post.objects.get(pk=post_id)
            if request.POST.get('remove_media') == 'on':
                post_obj.media = None

            form = self.form_class(
                request.POST,
                request.FILES,
                instance=post_obj
            )

        except Exception:
            pass

        if request.user == post_obj.user and form.is_valid():
            form.save()

        return redirect(request.META.get('HTTP_REFERER'))


class PostEditView(View):
    form_class = PostCreateForm

    def post(self, request, *args, **kwargs):

        post_id = kwargs.get('id')
        try:
            post_obj = Post.objects.get(pk=post_id)
            if request.POST.get('remove_media') == 'on':
                post_obj.media = None

            form = self.form_class(
                request.POST,
                request.FILES,
                instance=post_obj
            )

        except Exception:
            pass

        if request.user == post_obj.user and form.is_valid():
            form.save()

        return redirect(request.META.get('HTTP_REFERER'))


class PostDeleteView(View):

    def post(self, request, *args, **kwargs):

        post_id = kwargs.get('id')

        try:
            post_obj = Post.objects.get(pk=post_id)
        except Exception:
            pass

        if request.user == post_obj.user:
            post_obj.delete()

        return redirect('home_view')


class PostCommentView(View):

    def post(self, request, *args, **kwargs):
        post_id = kwargs.get('id')
        comment_text = request.POST.get('comment_text')

        Comment.objects.create(post_id=post_id, text=comment_text)

        return redirect(request.META.get('HTTP_REFERER'))


class CommentReplyView(View):
    form_class = CommentForm

    def post(self, request, *args, **kwargs):
        comment_id = kwargs.get('id')

        try:
            comment_obj = Comment.objects.get(id=comment_id)
            Comment.objects.create(
                text=request.POST.get('comment_text'),
                post=comment_obj.post,
                parent=comment_obj
            )
        except Exception:
            pass

        return redirect(request.META.get('HTTP_REFERER'))


class CommentDeleteView(View):

    def post(self, request, *args, **kwargs):
        comment_id = kwargs.get('id')

        try:
            comment_obj = Comment.objects.get(pk=comment_id)
            comment_obj.delete()
        except Exception:
            pass

        return redirect(request.META.get('HTTP_REFERER'))


class PostLikeView(View):

    def post(self, request, *args, **kwargs):
        post_id = kwargs.get('id')

        try:
            Like.objects.get(post_id=post_id, user=request.user)
        except Exception:
            Like.objects.create(post_id=post_id)

        return redirect(request.META.get('HTTP_REFERER'))


class PostUnlikeView(View):

    def post(self, request, *args, **kwargs):
        post_id = kwargs.get('id')

        try:
            like_obj = Like.objects.get(post_id=post_id, user=request.user)
            like_obj.delete()
        except Exception:
            pass

        return redirect(request.META.get('HTTP_REFERER'))


class NetworkView(View):
    template_name = 'home/network.html'

    def get(self, request, *args, **kwargs):

        try:
            connections = Connection.objects.filter(
                Q(user2_id=request.user.pk) | Q(user1_id=request.user.pk),
                is_connected=True
            ).count()
        except Exception:
            pass

        try:
            connections_received = Connection.objects.filter(
                user2_id=request.user.pk,
                is_connected=False
            )
        except Exception:
            pass

        try:
            connections_sent = Connection.objects.filter(
                user1_id=request.user.pk,
                is_connected=False
            )
        except Exception:
            pass

        context = {
            'connections': connections,
            'connections_received': connections_received,
            'connections_sent': connections_sent
        }

        return render(request, self.template_name, context)


class ViewAllConnectionsView(View):
    template_name = 'home/view_all_connections.html'

    def get(self, request, *args, **kwargs):

        try:
            connections1 = Connection.objects.filter(
                user2_id=request.user.pk,
                is_connected=True
            )
        except Exception:
            pass

        try:
            connections2 = Connection.objects.filter(
                user1_id=request.user.pk,
                is_connected=True
            )
        except Exception:
            pass

        connections = []
        for connection in connections1:
            connections.append(connection.user1)

        for connection in connections2:
            connections.append(connection.user2)

        connections.sort(key=lambda x: x.first_name)

        context = {'connections': connections}

        return render(request, self.template_name, context)
