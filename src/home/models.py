from django.db import models
from django.contrib.auth import get_user_model
from crum import get_current_user

User = get_user_model()


# Create your models here.
class Post(models.Model):
    text = models.CharField(max_length=400, null=True, blank=True)
    media = models.FileField(upload_to='post_images', blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, editable=False)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.pk)

    def save(self, *args, **kwargs):
        user = get_current_user()

        if user and not user.pk:
            user = None
        if not self.pk:
            self.user = user

        super(Post, self).save(*args, **kwargs)

    @property
    def likes_count(self):
        return self.like_set.count()

    @property
    def comments_count(self):
        return self.comment_set.count()

    class Meta:
        ordering = ['-updated_on']


class Comment(models.Model):
    text = models.CharField(max_length=140)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, editable=False)
    parent = models.ForeignKey(
        'self',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    commented_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.text

    def save(self, *args, **kwargs):
        user = get_current_user()

        if user and not user.pk:
            user = None
        if not self.pk:
            self.user = user

        super(Comment, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-commented_on']

    def get_comments(self):
        return Comment.objects.filter(parent=self)


class Like(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, editable=False)
    liked_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(True)

    def save(self, *args, **kwargs):
        user = get_current_user()

        if user and not user.pk:
            user = None
        if not self.pk:
            self.user = user

        super(Like, self).save(*args, **kwargs)


class Connection(models.Model):
    user1 = models.ForeignKey(
        User,
        related_name='connection_user',
        on_delete=models.CASCADE,
        editable=False
    )
    user2 = models.ForeignKey(
        User,
        related_name='connection_connection',
        on_delete=models.CASCADE
    )
    is_connected = models.BooleanField(default=False)
    is_requested = models.BooleanField(default=True)
    requested_on = models.DateTimeField(auto_now=True)
    connected_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.user1} ~ {self.user2}"

    def save(self, *args, **kwargs):
        user = get_current_user()

        if user and not user.pk:
            user = None
        if not self.pk:
            self.user = user

        super(Connection, self).save(*args, **kwargs)
