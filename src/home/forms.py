from django import forms
from .models import Post, Comment


class PostCreateForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('text', 'media')
        widgets = {
            'text': forms.TextInput(attrs={
                    'class': 'form-control form-control-sm',
                    'placeholder': 'What do you want to talk about?'
                }
            )
        }


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text', 'post',)
        widgets = {
            'text': forms.TextInput(attrs={
                    'class': 'form-control',
                    'placeholder': 'Post Comment',
                    'rows': '1'
                }
            )
        }
