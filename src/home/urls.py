from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import (
    HomeView,
    PostDetailView,
    PostEditView,
    PostDeleteView,
    PostLikeView,
    PostUnlikeView,
    PostCommentView,
    NetworkView,
    ViewAllConnectionsView,
    CommentReplyView,
    CommentDeleteView,
)


urlpatterns = [
    path('feed/', login_required(HomeView.as_view()), name='home_view'),
    path(
        'edit/<int:id>',
        login_required(PostEditView.as_view()),
        name='post_edit_view'
    ),
    path(
        'post/<int:id>',
        login_required(PostDetailView.as_view()),
        name='post_detail_view'
    ),
    path(
        'delete/post/<int:id>',
        login_required(PostDeleteView.as_view()),
        name='post_delete_view'
    ),
    path(
        'post/like/<int:id>',
        login_required(PostLikeView.as_view()),
        name='post_like_view'
    ),
    path(
        'post/unlike/<int:id>',
        login_required(PostUnlikeView.as_view()),
        name='post_unlike_view'
    ),
    path(
        'post/comment/<int:id>',
        login_required(PostCommentView.as_view()),
        name='post_comment_view'
    ),
    path(
        'comment/reply/<int:id>',
        login_required(CommentReplyView.as_view()),
        name='reply_comment_view'
    ),
    path(
        'comment/delete/<int:id>',
        login_required(CommentDeleteView.as_view()),
        name='comment_delete_view'
    ),
    path(
        'my-network/',
        login_required(NetworkView.as_view()),
        name='network_view'
    ),
    path(
        'view-all-connections/',
        login_required(ViewAllConnectionsView.as_view()),
        name='view_all_connections_view'
    ),
]
