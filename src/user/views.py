from django.db.models.query_utils import Q
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import View
from django.contrib.auth import get_user_model
from home.models import Connection
from .forms import UserEditForm, ChangeProfilePhotoForm


User = get_user_model()


# Create your views here.
class ProfileView(View):
    template_name_other = 'user/anonymous_profile.html'
    template_name_self = 'user/authenticated_profile.html'
    form_class = UserEditForm

    def get(self, request, *args, **kwargs):
        username = kwargs.get('username')

        try:
            user = User.objects.get(username=username)
        except Exception:
            return HttpResponse(username + ' not found.')

        my_context = {'user': user}

        try:
            connections = Connection.objects.filter(
                Q(user2_id=user.pk) | Q(user1_id=user.pk),
                is_connected=True
            ).count()
        except Exception:
            pass

        my_context['connections'] = connections

        if request.user.username == username:
            form = self.form_class(instance=request.user)
            my_context['form'] = form
            return render(request, self.template_name_self, my_context)

        connection_status = 0

        try:
            connection_obj = Connection.objects.get(
                Q(user1_id=request.user.pk) & Q(user2=user.id)
            )
            if connection_obj.is_requested:
                connection_status = 1
            elif connection_obj.is_connected:
                connection_status = 2
        except Exception:
            pass

        my_context['connection_status'] = connection_status

        return render(request, self.template_name_other, my_context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(
            request.POST,
            request.FILES,
            instance=request.user
        )

        if form.is_valid():
            form.save()

            return redirect('profile_view', request.user.username)

        return redirect(request.META.get('HTTP_REFERER'))


class ChangeProfilePhotoView(View):
    form_class = ChangeProfilePhotoForm

    def post(self, request, *args, **kwargs):
        if request.POST.get('remove_photo'):

            form = self.form_class(
                request.POST,
                instance=request.user
            )

            if form.is_valid():
                user_obj = User.objects.get(pk=request.user.id)
                user_obj.picture.delete()
                user_obj.picture = None
                user_obj.save()

            return redirect('profile_view', request.user.username)

        form = self.form_class(
            request.POST,
            request.FILES,
            instance=request.user
        )

        if form.is_valid():
            form.save()

        return redirect('profile_view', request.user.username)


class SearchProfileView(View):
    template_name = 'user/search_profile.html'

    def get(self, request, *args, **kwargs):

        search_term = request.GET.get('query')

        if search_term:
            users = User.objects.filter(
                username__contains=search_term
            )
        else:
            users = User.objects.none()

        search_results = []
        for user in users:
            connection_status = 0

            try:
                connection_obj = Connection.objects.get(
                    Q(user1_id=request.user.pk) & Q(user2_id=user.id)
                )
                if connection_obj.is_requested:
                    connection_status = 1
                elif connection_obj.is_connected:
                    connection_status = 2

            except Exception:
                pass

            search_results.append({
                'user_data': user,
                'connection_status': connection_status
            })

        context = {'search_results': search_results}

        return render(request, self.template_name, context)


class ConnectionRequestView(View):

    def post(self, request, *args, **kwargs):
        user_id = kwargs.get('id')

        try:
            Connection.objects.get(
                Q(user1_id=request.user.pk) & Q(user2=user_id)
            )
        except Exception:
            Connection.objects.create(
                user1_id=request.user.pk,
                user2_id=user_id
            )

        return redirect(request.META.get('HTTP_REFERER'))


class WithdrawConnectionRequestView(View):

    def post(self, request, *args, **kwargs):
        user_id = kwargs.get('id')

        try:
            connection_obj = Connection.objects.get(
                Q(user1_id=request.user.pk) & Q(user2=user_id)
            )
            connection_obj.delete()
        except Exception:
            pass

        return redirect(request.META.get('HTTP_REFERER'))


class AcceptConnectionRequestView(View):

    def post(self, request, *args, **kwargs):
        connection_id = kwargs.get('id')

        try:
            connection_obj = Connection.objects.get(pk=connection_id)
            connection_obj.is_requested = False
            connection_obj.is_connected = True
            connection_obj.save()
        except Exception:
            pass

        return redirect(request.META.get('HTTP_REFERER'))


class RemoveConnectionView(View):

    def post(self, request, *args, **kwargs):
        connection_id = kwargs.get('id')

        try:
            connection_obj = Connection.objects.get(pk=connection_id)
            connection_obj.delete()
        except Exception:
            pass

        return redirect(request.META.get('HTTP_REFERER'))
