from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import (
    ProfileView,
    SearchProfileView,
    ChangeProfilePhotoView,
    ConnectionRequestView,
    WithdrawConnectionRequestView,
    AcceptConnectionRequestView,
    RemoveConnectionView
)


urlpatterns = [
    path(
        'user/<str:username>/',
        login_required(ProfileView.as_view()),
        name='profile_view'
    ),
    path(
        'all/',
        login_required(SearchProfileView.as_view()),
        name='search_profile_view'
    ),
    path(
        'change/photo/',
        login_required(ChangeProfilePhotoView.as_view()),
        name='change_user_profile_photo'
    ),
    path(
        'connection-request/<int:id>/',
        login_required(ConnectionRequestView.as_view()),
        name='connection_request_view'
    ),
    path(
        'withdraw-connection-request/<int:id>/',
        login_required(WithdrawConnectionRequestView.as_view()),
        name='withdraw_connection_request_view'
    ),
    path(
        'accept-connection-request/<int:id>/',
        login_required(AcceptConnectionRequestView.as_view()),
        name='accept_connection_request_view'
    ),
    path(
        'remove-connection-request/<int:id>/',
        login_required(RemoveConnectionView.as_view()),
        name='remove_connection_view'
    ),
]
