from django.db import models
from django.contrib.auth.models import AbstractUser
from .managers import CustomUserManager


# Create your models here.
class User(AbstractUser):
    picture = models.ImageField(
        upload_to='profile_pictures',
        null=True,
        blank=True
    )
    email = models.EmailField(null=False, unique=True)
    headline = models.CharField(max_length=220, null=True, blank=True)
    education = models.CharField(max_length=100, null=True, blank=True)
    country = models.CharField(max_length=60, null=True, blank=True)
    location = models.CharField(max_length=120, null=True, blank=True)
    industry = models.CharField(max_length=100, null=True, blank=True)
    contact_info = models.CharField(max_length=220, null=True, blank=True)
    website = models.URLField(null=True, blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name', ]

    objects = CustomUserManager()

    def __str__(self):
        return self.email
