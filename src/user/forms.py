from django.contrib.auth import get_user_model
from django import forms

User = get_user_model()


class UserEditForm(forms.ModelForm):
    class Meta:
        model = User
        fields = (
            'picture',
            'first_name',
            'last_name',
            'username',
            'headline',
            'education',
            'country',
            'location',
            'industry',
            'contact_info',
            'website',
            )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].widget.attrs.update(
                {'class': 'form-control form-control-sm'}
            )


class ChangeProfilePhotoForm(forms.ModelForm):
    class Meta:
        model = User
        fields = (
            'picture',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].widget.attrs.update(
                {'class': 'form-control form-control-sm'}
            )
