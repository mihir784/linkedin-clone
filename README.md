# LinkedIn Clone Using Django

# Aim

Create a linkedin-clone with requirements specified in the following [user-stories](documentation/user-stories.txt) with this [database-schema](documentation/LinkedIn-DB-Schema.pdf).

## Prerequisites:

Make sure you have latest version of python and postgresql installed on your system.

# Setup the project

- Clone this repository into your local directory.
- Create a virtual environment, using the command:
  <br/>`python3 -m venv venv`

* Once the virtual environment is created, activate it using the command:
  <br/>`source venv/bin/activate`

### Requirements

File **requirements.txt** contains all requirements and dependencies, to install them run the command:
<br/>`pip install -r requirements.txt`

- Create the datbase in postgresql and change the **secret key** and database credentials in django-project's `settings.py` file.

```bash
  DATABASES = {
   'default': {
       'ENGINE': 'django.db.backends.postgresql_psycopg2',
       'NAME': '<database_name>',
       'USER': '<user_name>',
       'PASSWORD': '<password>',
       'HOST': '',
       'PORT': '',
   }
}
```

- After creating the database and changing the configurations run the following commands:

  `cd src/`

  `python3 manage.py makemigrations`
  
  `python3 manage.py migrate`

# Run the Server

- To start up the server run the command:
  `python3 manage.py runserver`

- Open the website on [localhost](http://127.0.0.1:8000/) and check it out.
- For the live version visit [this link](http://linkedin-clone-mihir.herokuapp.com/).
